from django.contrib import admin

from .models import Book, BookItem, Genre, Author, Borrow, Reserve, Queue
from .forms import CreateBookForm, CreateBookItemForm

from django.contrib import messages

class BookAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title', 'year', 'publisher')}

    def get_form(self, request, obj=None, **kwargs):
        """
        Serve specific form for creating Book objects
        
        With creating we give possibility to generate book items of book which is being created
        """
        if obj is None:
            return CreateBookForm
        else:
            return super(BookAdmin, self).get_form(request, obj, **kwargs)
        
    def save_model(self, request, obj, form, change):
        """
        Generate items after creating a book
        """
        obj.save()
        if (not change) and form.cleaned_data['generate_items'] == True:
            for i in range(int(form.cleaned_data['total_items'])):
                BookItem.objects.create(book = obj)
        
class BookItemAdmin(admin.ModelAdmin):

    search_fields = ["book__title", "book__authors__name", "book__publisher", "book__genre__name", "book__year", "id"]

    def get_form(self, request, obj=None, **kwargs):
        """
        Serve specific form for creating BookItem objects
        
        With creating we give possibility to generate multiple items with 'one click'
        """
        if obj is None:
            return CreateBookItemForm
        else:
            return super(BookItemAdmin, self).get_form(request, obj, **kwargs)
        
    def save_model(self, request, obj, form, change):
        """
        Generate more items (if necessary) after creating an item
        """
        obj.save()
        if (not change) and int(form.cleaned_data['total_items']) > 1:
            for i in range(int(form.cleaned_data['total_items'])-1):
                BookItem.objects.create(book = obj.book)

class ReserveAdmin(admin.ModelAdmin):
    """ Admin class responsible for managing Reserve model. """

    search_fields = ('reader__pesel',)
    readonly_fields = ('reader', 'book_item', 'date')

    def has_add_permission(self, request, obj=None):
        """ Indicates that Admin has no add permissions. """
        return False

    def from_reserve_to_borrow(self, request, queryset):
        """ Moves selected Reserve items to Borrow.
            
            request: request object
            queryset: items to be moved
            """
        from datetime import datetime
        # maybe there's cleaner solution
        for x in queryset:
            cpy = Borrow()
            cpy.book_item = x.book_item
            cpy.reader = x.reader
            cpy.date = datetime.now() 
            x.delete()
            cpy.save()
    from_reserve_to_borrow.short_description = "Move from Reserved to Borrowed"
    actions = [from_reserve_to_borrow]


class BorrowAdmin(admin.ModelAdmin):
    """ Admin class responsible for managing Reserve model. """
    search_fields = ('reader__pesel',)
    readonly_fields = ('reader', 'book_item', 'date')

    def has_add_permission(self, request, obj=None):
        """ Indicates that Admin has no add permissions. """
        return False

class QueueAdmin(admin.ModelAdmin):
    """ Admin panel for queued books """
    def from_queue_to_borrow(self, request, queryset):
        """ Moves selected Queue items to Borrow.

            request: request object
            queryset: items to be moved
            """
        from datetime import datetime
        # maybe there's cleaner solution
        for x in queryset:
            if len(Borrow.objects.filter(book_item__id=x.book_item.id)) == 0 and len(Reserve.objects.filter(book_item__id=x.book_item.id)) == 0:
                cpy = Borrow()
                cpy.book_item = x.book_item
                cpy.reader = x.reader
                cpy.date = datetime.now()
                x.delete()
                cpy.save()
            else:
                messages.error(request, "You can not borrow a book that is not available.")
    from_queue_to_borrow.short_description = "Move from Queue to Borrowed (if is available, if not - nothing changes)"
    actions = [from_queue_to_borrow]

admin.site.register(Book, BookAdmin)
admin.site.register(BookItem, BookItemAdmin)
admin.site.register(Genre)
admin.site.register(Author)
admin.site.register(Borrow, BorrowAdmin)
admin.site.register(Reserve, ReserveAdmin)
admin.site.register(Queue, QueueAdmin)
