from django.db import models
from readers.models import Reader


class Genre(models.Model):
    name = models.CharField(max_length=64)

    def __str__(self):
        return self.name


class Author(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Book(models.Model):
    title = models.CharField(max_length=512)
    authors = models.ManyToManyField(Author)
    publisher = models.CharField(max_length=255)
    year = models.PositiveSmallIntegerField()
    genre = models.ManyToManyField(Genre)
    slug = models.SlugField(max_length=100)

    def __str__(self):
        info = self.title + ' | '
        for author in self.authors.all():
            info = info + author.name + ', '
        if info[-2:] == ', ':
            info = info[0:-2]
        return info

    def get_absolute_url(self):
        return "/books/details/{slug}/".format(slug=self.slug)


class BookItem(models.Model):
    book = models.ForeignKey(Book)

    def __str__(self):
        return "Book item no #" + str(self.pk) + ": " + str(self.book)


class Reserve(models.Model):
    book_item = models.ForeignKey(BookItem)
    reader = models.ForeignKey(Reader)
    date = models.DateField()

    def __str__(self):
        return "Reservation of " + str(self.book_item) + " done by: " + str(self.reader) + " on " + str(self.date)


class Borrow(models.Model):
    book_item = models.ForeignKey(BookItem)
    reader = models.ForeignKey(Reader)
    date = models.DateField()
    
    def __str__(self):
        return "Borrow of " + str(self.book_item) + " done by: " + str(self.reader) + " on " + str(self.date)


class Queue(models.Model):
    book_item = models.ForeignKey(BookItem)
    reader = models.ForeignKey(Reader)
    date = models.DateField(auto_now_add=True)

    def __str__(self):
        return "Queue of " + str(self.book_item) + " done by: " + str(self.reader) + " on " + str(self.date)