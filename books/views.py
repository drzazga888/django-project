from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from .models import Reader
from .models import Book
from .models import BookItem
from .models import Reserve
from .models import Borrow
from .models import Queue
from .forms import BookSearchForm
from django.db.models import Q
from functools import reduce
from django.contrib.auth.decorators import login_required
import datetime

from urllib.request import urlopen
from bs4 import BeautifulSoup
from django.template.defaultfilters import slugify


def book_search(request):
    
    if request.method == 'GET' and request.GET:
        formSearch = BookSearchForm(request.GET)
        if formSearch.is_valid():
            # Create queries
            bookset = Book.objects.all()
            
            # by author
            queried_authors_list = formSearch.cleaned_data['authors'].split(',')
            if queried_authors_list:
                # Encapsulating queries like "SELECT * FROM Book WHERE authors IN queried_authors_list" into Q objects and
                # 'OR'ing all Qs in order to obtain full query
                bookset = bookset.filter(reduce(lambda x, y: x | y, [Q(authors__name__contains=a) for a in queried_authors_list]))
		
            # by title
            if formSearch.cleaned_data['title']:
                bookset = bookset.filter(title__contains=formSearch.cleaned_data['title'])

            # and finally by genre
            queried_genres_list = formSearch.cleaned_data['genres'].split(',')
            if queried_genres_list:
                bookset = bookset.filter(reduce(lambda x, y: x | y, [Q(genre__name__contains=g) for g in queried_genres_list]))

            return render(request, 'books/search.html', {'bookset': set(bookset), 'formSearch': formSearch})

    else:
        formSearch = BookSearchForm()

    return render(request, 'books/search.html', {'formSearch': formSearch})

@login_required
def book_details(request, slug):
    """
    Show details of single book item and allow booking
    """
    
    # Obtain requested title from database based on slug
    try:
        requested_book = Book.objects.get(slug=slug)
    except Book.DoesNotExist:
        return render(request, 'books/book_details.html', {'requested_book' : None})

    # Obtain full list of items of the requested book
    item_set = BookItem.objects.filter(book__slug=slug)
    no_total = len(item_set)

    # Calculate the number of available items based on the number of borrowed and reserved ones
    # Mind the case when the collective length of both lists exceeds the overall quantity.
    # It happens when there are more reservations than non-away items and it requires queueing the reservation somehow.
    borrow_set = Borrow.objects.filter(book_item__in=item_set)
    reserve_set = Reserve.objects.filter(book_item__in=item_set)
    no_available = no_total - len(borrow_set) - len(reserve_set)

	# Check if user has already borrowed or reserved the requested book
    try:
        borrowed = Borrow.objects.get(reader__user__username=request.user.username, book_item__book__slug=slug)
    except Borrow.DoesNotExist:
        borrowed = None

    try:
        reserved = Reserve.objects.get(reader__user__username=request.user.username, book_item__book__slug=slug)
    except Reserve.DoesNotExist:
        reserved = None

    try:
        queued = Queue.objects.get(reader__user__username=request.user.username, book_item__book__slug=slug)
    except Queue.DoesNotExist:
        queued = None
	
    # User has a limited amount of time to read the book, after which they should either bring it back or prolong it if possible.
    expiry = datetime.date.today()
    if borrowed:
        time_limit = 30
        expiry = borrowed.date + datetime.timedelta(days=time_limit)

	# If user has requested a book, they should borrow it during some period of time (say a week)
    reservation_expiry = datetime.date.today()
    if reserved:
        days_forward = 7
        reservation_expiry = reserved.date + datetime.timedelta(days=days_forward)

    # If user has not reserved the book yet and there is at least one free item, they can do it instantly
    available_item = None
    queue_item = None
    if not reserved and no_available > 0:
        # Retrieve any of the available BookItem objects.
        # It will be ready for wrapping into a Reserve object should user decide to reserve the book.
        reserved_item_set = [reserve.book_item for reserve in reserve_set]
        borrowed_item_set = [borrow.book_item for borrow in borrow_set]
        available_item_set = [item for item in item_set if (item not in reserved_item_set and item not in borrowed_item_set)]
        available_item = available_item_set[0]
    elif no_available == 0:
        queue_item = requested_book

    # Getting image of the book from Ceneo.pl site:
    # 1) compose url to find proper images
    url_to_parse = "http://www.ceneo.pl/;szukaj-" + slugify([requested_book.title, requested_book.authors.all()[0].name]).replace('-','+')
    # 2) render this papge and get the source
    sock = urlopen(url_to_parse)
    html_source = sock.read()
    sock.close()
    # 3) create parser object based on html source
    soup = BeautifulSoup(html_source)
    # 4) find <div> with book cover image
    single_book_div = soup.find_all("div", class_="cat-prod-row-foto")
    # 5) find source of the first image in this <div>
    if single_book_div:
        book_img_src = "http:" + single_book_div[0].find_all("img")[0].get("src")
    else:
        book_img_src = ""

    context = {'requested_book': requested_book,
        'no_total': no_total,
        'no_available': no_available,
        'book_img_src': book_img_src,
        'expiry': expiry,
        'reservation_expiry': reservation_expiry,
        'borrowed': borrowed,
        'reserved': reserved,
        'queued': queued,
        'available_item': available_item,
        'queue_item': queue_item}
    return render(request, 'books/book_details.html', context)
		
@login_required
def reserve(request):
    if request.method == 'POST' and request.POST:
        # Retrieve POST data: requested book item string representation and its title
        available_item_str = request.POST.get("available_item_str")
        available_item_title = request.POST.get("available_item_title")

        # From the whole set of requested title items choose the one matching the string representation
        # I'm aware it's a bit clumsy.
        available_item = None
        for bookitem in BookItem.objects.filter(book__title=available_item_title):
            if str(bookitem) == available_item_str:
                available_item = bookitem
                break

        # Save the Reserve object to database
        reservation_expiry = None
        if available_item:
            Reserve(book_item=available_item, reader=Reader.objects.get(user__username=request.user.username), date=datetime.date.today()).save()
            days_forward=7 # Time to check the book out
            reservation_expiry = datetime.date.today() + datetime.timedelta(days=days_forward)

        return render(request, 'books/reservation_done.html', {'available_item': available_item, 'reservation_expiry': reservation_expiry})

@login_required
def add2queue(request):
    if request.method == 'POST' and request.POST:
        queue_item_str = request.POST.get("queue_item_str")
        queue_item_title = request.POST.get("queue_item_title")
        borrowed = Borrow.objects.filter(book_item__book__title=queue_item_title).order_by('-date')
        reserved = Reserve.objects.filter(book_item__book__title=queue_item_title).order_by('-date')

        book = None
        if borrowed and reserved:
            if borrowed[0].date < reserved[0].date:
                book = borrowed[0]
            else:
                book = reserved[0]
        elif not borrowed:
            book = reserved[0]
        elif not reserved:
            book = borrowed[0]

        if book:
            queue = Queue.objects.create(book_item=book.book_item, reader=Reader.objects.get(user=request.user))
            queue.save()

    return render(request, "books/queue_done.html", {
        "book_title": queue_item_title
    })

@login_required
def cancel_reservation(request):
    if request.method == 'POST' and request.POST:

        requested_book_title = request.POST["requested_book_title"]
        requested_book_publisher = request.POST["requested_book_publisher"]
        requested_book_year = request.POST["requested_book_year"]
        if request.user.username == 'admin':
            nick = request.POST["nick"]
        else:
            nick = request.user.username

        # Delete the entry from Reserve table which matches given parameters
        reserve = Reserve.objects.get(book_item__book__title=requested_book_title, book_item__book__publisher=requested_book_publisher, book_item__book__year=requested_book_year, reader__user__username=nick)

        if request.POST['name'] == "cancel":

            reserve.delete()

            return render(request, 'books/reservation_canceled.html', {'requested_book_title': requested_book_title})
        elif request.POST['name'] == "add":
            borrowed = Borrow()
            borrowed.book_item = reserve.book_item
            borrowed.date = reserve.date
            borrowed.reader = reserve.reader
            borrowed.save()

            reserve.delete()

            return render(request, 'books/reservation_canceled.html', {'requested_book_title': requested_book_title})


@login_required
def cancel_borrow(request):
    if request.method == 'POST' and request.POST:

        requested_book_title = request.POST["requested_book_title"]
        requested_book_publisher = request.POST["requested_book_publisher"]
        requested_book_year = request.POST["requested_book_year"]
        if request.user.username == 'admin':
            nick = request.POST["nick"]
        else:
            nick = request.user.username

        # Delete the entry from Reserve table which matches given parameters
        borrow = Borrow.objects.get(book_item__book__title=requested_book_title, book_item__book__publisher=requested_book_publisher, book_item__book__year=requested_book_year, reader__user__username=nick)

        if request.POST['name'] == "cancel":

            borrow.delete()

            return render(request, 'books/reservation_canceled.html', {'requested_book_title': requested_book_title})
        # elif request.POST['name'] == "add":
        #     print(">add")
        #     borrowed = Borrow()
        #     borrowed.book_item = reserve.book_item
        #     borrowed.date = reserve.date
        #     borrowed.reader = reserve.reader
        #     borrowed.save()
        #
        #     borrow.delete()
        #
        #     return render(request, 'books/reservation_canceled.html', {'requested_book_title': requested_book_title})

@login_required
def cancel_queue(request):
    if request.method == 'POST' and request.POST:

        queue_id = request.POST["queue_id"]
        nick = request.user.username

        # Delete the entry from Reserve table which matches given parameters
        queue = Queue.objects.get(id=queue_id, reader__user__username=nick)
        book_title = queue.book_item.book.title
        queue.delete()

        return render(request, 'books/queue_canceled.html', {'book_title': book_title})