from django.test import TestCase
from .forms import BookSearchForm
from .models import *
from django.contrib.auth.models import User
import datetime

class BooksViewsTestCase(TestCase):

    def setUp(self):
        self.title = "Język C++. Szkoła programowania."
        self.unknown_title = "Thinking in Java."
        self.authors = Author.objects.create(name="Stephen Prata")
        self.publisher = "Helion"
        self.year = 2012
        self.genre = Genre.objects.create(name="Programowanie")
        self.slug = "jezyk-c-szkola-programowania-2012-helion"
        self.unknown_slug = "thinking-in-java-2011-helion"
        self.book = Book.objects.create(title=self.title, publisher=self.publisher, year=self.year, slug=self.slug)
        self.book.authors.add(self.authors)
        self.book.genre.add(self.genre)
        self.book.save()
        self.bookItem = BookItem(book=self.book)
        self.bookItem.save()
        self.user = User.objects.create_user('temporary', 'temporary@example.com','temporary')
        self.reader = Reader.objects.create(user=self.user, pesel="64011613697", address="ul. Krakowska 12/36", city="Kraków", postal_code="30-333")

    def test_bookSearchForm_blank(self):
        form = BookSearchForm()
        self.assertFalse(form.is_valid())

    def test_bookSearchForm_complete(self):
        form = BookSearchForm( {'title' : self.title} )
        self.assertTrue(form.is_valid())

    def test_book_search_blank_form(self):
        resp = self.client.get('/books/search/')
        self.assertEqual(resp.status_code, 200)
        self.assertTrue('formSearch' in resp.context)
        self.assertFalse('bookset' in resp.context)

    def test_book_search_complete_form(self):
        resp = self.client.get('/books/search/', {'title' : self.title})
        self.assertEqual(resp.status_code, 200)
        self.assertTrue('formSearch' in resp.context)
        self.assertTrue('bookset' in resp.context)
        self.assertTrue(len(resp.context['bookset']) > 0)
        for book in resp.context['bookset']:
            self.assertEqual(book.title, self.title)

    def test_book_search_complete_form_non_existent_title(self):
        resp = self.client.get('/books/search/', {'title' : self.unknown_title})
        self.assertEqual(resp.status_code, 200)
        self.assertTrue('formSearch' in resp.context)
        self.assertTrue('bookset' in resp.context)
        self.assertTrue(len(resp.context['bookset']) == 0)

    def test_reserve_not_logged_in_redirects(self):
        resp = self.client.post('/books/reserve/', {
            'available_item_str' : str(self.bookItem),
            'available_item_title' : self.title
        })
        self.assertEqual(resp.status_code, 302)
        self.assertIsNone(resp.context)

    def test_reserve_logged_in(self):
        self.client.login(username="temporary", password="temporary")
        try:
            reserved = Reserve.objects.get(reader__user__username=self.user.username, book_item__book__slug=self.slug)
        except Reserve.DoesNotExist:
            reserved = None
        self.assertIsNone(reserved)
        resp = self.client.post('/books/reserve/', {
            'available_item_str' : str(self.bookItem),
            'available_item_title' : self.title
        })
        self.assertEqual(resp.status_code, 200)
        self.assertTrue('available_item' in resp.context)
        self.assertTrue('reservation_expiry' in resp.context)
        self.assertEqual(resp.context['available_item'], self.bookItem)
        self.assertEqual(resp.context['reservation_expiry'], datetime.date.today() + datetime.timedelta(days=7))
        try:
            reserved = Reserve.objects.get(reader__user__username=self.user.username, book_item__book__slug=self.slug)
        except Reserve.DoesNotExist:
            reserved = None
        self.assertIsNotNone(reserved)

    def test_cancel_reservation_not_logged_in_redirects(self):
        self.client.post('/books/reserve/', {
            'available_item_str' : str(self.bookItem),
            'available_item_title' : self.title
        })
        resp = self.client.post('/books/reservation_canceled/', {
            'requested_book_title' : self.title,
            'requested_book_publisher' : self.publisher,
            'requested_book_year' : self.year,
        })
        self.assertEqual(resp.status_code, 302)
        self.assertIsNone(resp.context)

    def test_cancel_reservation_logged_in(self):
        self.client.login(username="temporary", password="temporary")
        self.client.post('/books/reserve/', {
            'available_item_str' : str(self.bookItem),
            'available_item_title' : self.title
        })
        try:
            reserved = Reserve.objects.get(reader__user__username=self.user.username, book_item__book__slug=self.slug)
        except Reserve.DoesNotExist:
            reserved = None
        self.assertIsNotNone(reserved)
        resp = self.client.post('/books/reservation_canceled/', {
            'requested_book_title' : self.title,
            'requested_book_publisher' : self.publisher,
            'requested_book_year' : self.year,
        })
        self.assertEqual(resp.status_code, 200)
        self.assertTrue('requested_book_title' in resp.context)
        self.assertEqual(resp.context['requested_book_title'], self.title)
        try:
            reserved = Reserve.objects.get(reader__user__username=self.user.username, book_item__book__slug=self.slug)
        except Reserve.DoesNotExist:
            reserved = None
        self.assertIsNone(reserved)

    def test_book_details_not_logged_in_redirects(self):
        resp = self.client.get('/books/details/'+self.slug+'/')
        self.assertEqual(resp.status_code, 302)

    def test_book_details_logged_in(self):
        self.client.login(username="temporary", password="temporary")
        resp = self.client.get('/books/details/'+self.slug+'/')
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.context['requested_book'], self.book)
        self.assertEqual(resp.context['no_total'], 1)
        self.assertEqual(resp.context['no_available'], 1)
        self.assertIsNone(resp.context['borrowed'])
        self.assertIsNone(resp.context['reserved'])
        self.assertEqual(resp.context['available_item'], self.bookItem)

    def test_book_details_reserved(self):
        self.client.login(username="temporary", password="temporary")
        self.client.post('/books/reserve/', {
            'available_item_str' : str(self.bookItem),
            'available_item_title' : self.title
        })
        resp = self.client.get('/books/details/'+self.slug+'/')
        self.assertIsNotNone(resp.context['reserved'])
        self.assertIsNone(resp.context['borrowed'])

    def test_book_details_borrowed(self):
        self.client.login(username="temporary", password="temporary")
        Borrow(book_item=self.bookItem, reader=self.reader, date=datetime.date.today()).save()
        resp = self.client.get('/books/details/'+self.slug+'/')
        self.assertIsNone(resp.context['reserved'])
        self.assertIsNotNone(resp.context['borrowed'])

    def test_details_non_existent_book(self):
        self.client.login(username="temporary", password="temporary")
        resp = self.client.get('/books/details/'+self.unknown_slug+'/')
        self.assertIsNone(resp.context['requested_book'])

    def test_details_empty_slug(self):
        self.client.login(username="temporary", password="temporary")
        resp = self.client.get('/books/details//')
        self.assertIsNone(resp.context['requested_book'])