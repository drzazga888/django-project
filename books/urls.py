from django.conf.urls import url

urlpatterns = [
    url(r'^search/$', 'books.views.book_search'),
    url(r'^details/(?P<slug>[-\w]*)/$', 'books.views.book_details'),
    url(r'^reserve/$', 'books.views.reserve'),
    url(r'^add2queue/$', 'books.views.add2queue'),
    url(r'^reservation_canceled/$', 'books.views.cancel_reservation'),
    url(r'^borrow_canceled/$', 'books.views.cancel_borrow'),
    url(r'^queue_canceled/$', 'books.views.cancel_queue'),
]