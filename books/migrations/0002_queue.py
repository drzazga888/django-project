# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('readers', '0002_auto_20150706_1334'),
        ('books', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Queue',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('date', models.DateField(auto_now_add=True)),
                ('book_item', models.ForeignKey(to='books.BookItem')),
                ('reader', models.ForeignKey(to='readers.Reader')),
            ],
        ),
    ]
