from django import forms
from .models import Book, BookItem
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Div, Field
from crispy_forms.bootstrap import FormActions

class BookSearchForm(forms.Form):
    title = forms.CharField(max_length=512, required=False)
    authors = forms.CharField(max_length=256, required=False)
    genres = forms.CharField(max_length=256, required=False)

    def __init__(self, *args, **kwargs):
        super(BookSearchForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_tag = False
        self.helper.form_show_labels = False
        self.helper.disable_csrf = True
        self.helper.layout = Layout(
            Div(
                Div(
                    Div(
                        Field('title', placeholder=kwargs.pop('query_placeholder', 'Title')),
                        css_class = 'col-sm-6 col-md-3 col-md-offset-1',
                    ),
                    Div(
                        Field('authors', placeholder=kwargs.pop('query_placeholder', 'Authors')),
                        css_class = 'col-sm-6 col-md-3',
                    ),
                    Div(
                        Field('genres', placeholder=kwargs.pop('query_placeholder', 'Genres')),
                        css_class = 'col-sm-6 col-md-3',
                    ),
                    FormActions(
                        Submit('submit', 'Search'),
                        css_class = 'col-md-1',
                    ),
                ),
                css_class = 'row text-center',
            ),
        )
        self.fields['title'].widget.attrs['autofocus'] = 'autofocus'
    
class CreateBookForm(forms.ModelForm):

    error_messages = {
        'non_positive_number': "Number of items to generate must be positive integer value.",
    }

    class Meta:
        model = Book
        fields = ['title', 'authors', 'publisher', 'year', 'genre', 'slug']
        
    generate_items = forms.BooleanField(required=False, initial=False)
    total_items = forms.IntegerField(required=False, label='Number of items', initial=0)
    
    def clean_total_items(self):
        # Validate total_items field only if user want to generate items
        if self.cleaned_data['generate_items'] == True:
            if self.cleaned_data['total_items'] == None or (self.cleaned_data['total_items'] != None and int(self.cleaned_data['total_items']) < 1):
                raise forms.ValidationError(
                    self.error_messages['non_positive_number'],
                    code='non_positive_number',
                )
        return self.cleaned_data['total_items']
        
    
class CreateBookItemForm(forms.ModelForm):

    error_messages = {
        'non_positive_number': "Number of items to generate must be positive integer value.",
    }

    class Meta:
        model = BookItem
        fields = ['book']
        
    total_items = forms.IntegerField(required=True, initial=1)
    
    def clean_total_items(self):
        if self.cleaned_data['total_items'] == None or (self.cleaned_data['total_items'] != None and int(self.cleaned_data['total_items']) < 1):
            raise forms.ValidationError(
                self.error_messages['non_positive_number'],
                code='non_positive_number',
            )
        return self.cleaned_data['total_items']
        
