# Informacja do jakiego brancha kto należy #
Branche:

 - oplaty => Kasia 
 - propozycja_ksiazek => Kamil 
 - sprzedaz_ksiazek => Kamil + Adrian
 - prolongata => Dawid + Adrian
 - kolejka_zamowien => Dawid

Tomek już odwalił swoją robotę (nieładnie że na głównym branchu, będzie kara)oje commity mają usuwać moje 

"To Kamil, że zrobiłem coś na głównym branchu, nie oznacza, że Twoje commity mają usuwać MOJE zmiany!!!!!!"

# Lib.rar #
  
Multipurpose library, mainly for coders.

(Project under development, more info on [wiki](https://bitbucket.org/drzazga888/django-project/wiki/Home))

# Authors #
(first half of project, alphabetical by last name)  

  • Bartosz Kahl  
  • Paweł Kaim  
  • Bartłomiej Meder  
  • Dawid Sas  
  • Mateusz Wysocki 

(second half of project, non-alphabetical because we're lazy)  

  • Dawid Karmiński  
  • Tomasz Bajorek  
  • Katarzyna Herman  
  • Adrian Dołubizno  
  • Kamil Drzazga  

# Quick hands-on guide: #
1. Resolve dependencies:  
• django-localflavor  
• django-crispy-forms  
• beautifulsoup4  
2. Take a look at the [wiki](https://bitbucket.org/drzazga888/django-project/wiki/Home)  
You will find documentation and link for trello board.  
  
3. Download repository