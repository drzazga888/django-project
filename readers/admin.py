from django.contrib import admin
from django.contrib.auth.models import User
from .models import Reader, Suggestion
from .forms import ReaderForm, SuggestionForm


def activate_user(modeladmin, request, queryset):
    queryset.update(is_active=True)
activate_user.short_description = "Activate selected users"

from .models import Reader, SellingItem
from .forms import ReaderForm


class ReaderAdmin(admin.ModelAdmin):
    """
    Substitute custom form to provide dedicated PESEL and postal-code fields
    """
    form = ReaderForm


class UserAdmin(admin.ModelAdmin):
    list_display = ('username','first_name', 'last_name', 'email', 'is_active')
    actions = [activate_user]

admin.site.unregister(User)
admin.site.register(User, UserAdmin)

class SuggestionsAdmin(admin.ModelAdmin):
    """
    Support for managing suggestions
    """
    #form = SuggestionForm
    pass

admin.site.register(Suggestion, SuggestionsAdmin)
admin.site.register(Reader, ReaderAdmin)

def accept_selling_item(modeladmin, request, queryset):
    queryset.update(admin_confirmed=True)
accept_selling_item.short_description = "Accept selected selling items"

class SellingItemAdmin(admin.ModelAdmin):
    list_display = ('book_title', 'book_price', 'book_status', 'user_confirmed', 'admin_confirmed')
    readonly_fields = ('user_confirmed',)
    actions = [accept_selling_item]

admin.site.register(SellingItem, SellingItemAdmin)

