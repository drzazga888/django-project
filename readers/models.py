from django.db import models
from django.contrib.auth.models import User


class Reader(models.Model):
    user = models.OneToOneField(User)
    pesel = models.CharField(max_length=11)
    address = models.CharField(max_length=128)
    city = models.CharField(max_length=64)
    postal_code = models.CharField(max_length=6)

    def __str__(self):
        return self.user.first_name + " " + self.user.last_name + " (" + self.user.username + "), PESEL: " + self.pesel


class Suggestion(models.Model):
    user = models.ForeignKey(User)
    book_title = models.CharField(max_length=512)
    book_url = models.URLField(max_length=512)
    suggest_reason = models.CharField(max_length=512)

    def __str__(self):
        return "Suggestion made by " + str(self.user.username) +\
               " to book: " + str(self.book_title) +\
               " with reference to " + str(self.book_url) +\
               " with reason " + str(self.suggest_reason)


BOOK_STATUS = (
    (5, 'Excellent'),
    (4, 'Good'),
    (3, 'Normal'),
    (2, 'Bad'),
    (1, 'Ugly'),
)


class SellingItem(models.Model):
    user = models.ForeignKey(User)
    book_title = models.CharField(max_length=512)
    book_authors = models.CharField(max_length=512)
    book_publisher = models.CharField(max_length=128)
    book_url = models.URLField(max_length=512)
    book_status = models.IntegerField(choices=BOOK_STATUS)
    book_price = models.DecimalField(max_digits=6, decimal_places=2)
    date = models.DateField(auto_now_add=True)
    admin_confirmed = models.BooleanField(default=False)
    user_confirmed = models.BooleanField(default=False)

    def __str__(self):
        book_status = None
        for status in BOOK_STATUS:
            if status[0] == self.book_status:
                book_status = status[1]
                break
        return str(self.date) + ": " + str(self.user.username) +\
            " wants to sell " + str(self.book_title) +\
            ", authors: " + str(self.book_authors) +\
            ", publisher: " + str(self.book_publisher) +\
            ", status: " + str(book_status) +\
            " (" + str(self.book_status) +\
            "/" + str(len(BOOK_STATUS)) + ")"

