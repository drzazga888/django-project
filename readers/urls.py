from django.conf.urls import include, url
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^$', 'readers.views.personal_info'),
    url(r'^suggest/$', 'readers.views.suggest'),
    url(r'^sell/$', 'readers.views.sell'),
    url(r'^sell/confirm_yes/(?P<id>[0-9]+)/$', 'readers.views.sell_confirm_yes'),
    url(r'^sell/confirm_no/(?P<id>[0-9]+)/$', 'readers.views.sell_confirm_no'),
    url(r'^create/$', 'readers.views.create'),
    url(r'^info/$', 'readers.views.personal_info'),
    url(r'^items_info/$', 'readers.views.items_info'),
    url(r'^login/$', auth_views.login, {'template_name': 'readers/login.html'}),
    url(r'^logout/$', auth_views.logout, {'template_name': 'readers/logged_out.html'}),
    url(r'^change/$', 'readers.views.personal_change'),
    url(r'^change_email/$', 'readers.views.change_email'),
    url(r'^change_password/$', 'readers.views.change_password'),
    url(r'^users/$', 'readers.views.users'),
]
