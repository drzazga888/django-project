from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Reader, SellingItem
from books.models import Reserve, Queue
from books.models import Borrow
from django.contrib.auth.models import User
from .forms import (ReaderForm, UserForm, EmailForm, PasswordForm, ChangeReaderForm, ChangeUserForm,
                    SuggestionForm, SellingItemForm)
import datetime

@login_required
def personal_info(request):
    """
    Show [and modify - TODO] personal details of the Reader
    """
    message = request.session.get('message', False)
    if message:
        del request.session['message']

    try:
        reader = Reader.objects.get(user__username=request.user.username)
    except Reader.DoesNotExist:
        reader = None
        
    return render(request, 'readers/personal_info.html', {'reader': reader, 'message':message})

@login_required
def personal_change(request):
    message = request.session.get('message', False)
    if message:
        del request.session['message']
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        formR = ChangeReaderForm(request.POST)
        formU = ChangeUserForm(request.POST)
        # check whether it's valid:
        if formR.is_valid() and formU.is_valid():
            formU.save(request.user.username)
            formR.save(request.user.id)
            return redirect('/account/info')
        elif formU.is_valid() == False:
            message = formU.errors
        elif formR.is_valid() == False:
            message = formR.errors
    else:
        user = User.objects.get(username=request.user)
        default_udata = {'first_name':user.first_name, 'last_name':user.last_name}
        reader = Reader.objects.get(user_id=user.id)
        default_rdata = {'pesel':reader.pesel, 'address':reader.address, 'postal_code':reader.postal_code, 'city':reader.city}
        formR = ChangeReaderForm(default_rdata)
        formU = ChangeUserForm(default_udata)
    return render(request, 'readers/personal_change.html', {'formU': formU, 'formR': formR, 'message':message})

@login_required
def suggest(request):
    """
    Allows users to make a suggestion about adding new book to store.
    Require login.
    This method renders a form which must be filled.
    Saves our suggestion about book to db
    Afterwards admin receive info about adding new book.
    He can reject it or manage it.
    """
    message = ''
    message_class = ''
    if request.POST.get('book_url'):
        form = SuggestionForm(request.POST)
        if form.is_valid():
            form.save(request.user.id)
            message = 'Suggestion added! Thank you for supporting us.'
            message_class = 'success'
        else:
            message = form.errors
            message_class = 'danger'
    return render(request, 'readers/suggest_form.html', {
        'form': SuggestionForm(),
        'message': message,
        'message_class': message_class
    })

@login_required
def change_email(request):
    if request.method == 'POST':
        form = EmailForm(request.POST)
        if form.is_valid():
            form.save(request.user.username)
        else:
            request.session['message'] = form.errors
        return redirect('readers.views.personal_info')

@login_required
def change_password(request):
    if request.method == 'POST':
        form = PasswordForm(request.POST)
        if form.is_valid():
            form.save(request.user.username)
        else:
            request.session['message'] = form.errors
        return redirect('readers.views.personal_info')

@login_required
def sell(request):
    """
    Allows to sell book to lib.rar book store
    """
    message = ''
    message_class = ''
    if request.POST.get('book_price'):
        form = SellingItemForm(request.POST)
        if form.is_valid():
            form.save(request.user.id)
            message = 'You just added a book selling offer to us!'
            message_class = 'success'
        else:
            message = form.errors
            message_class = 'danger'
    return render(request, 'readers/selling_form.html', {
        'form': SellingItemForm(),
        'message': message,
        'message_class': message_class
    })

def create(request):
    """
    Creates a new library account (create User-Reader pair objects)
    """
    
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        formR = ReaderForm(request.POST)
        formU = UserForm(request.POST)
        # check whether it's valid:
        if formR.is_valid() and formU.is_valid():
            # create User object based on form:
            user = formU.save()
            
            # create Reader object based on form, but without saving to database (commit attribute):
            reader = formR.save(commit=False)
            # link reader with just created user:
            reader.user = user
            # finally save new reader:
            reader.save()
            
            # render confirm message:
            return render(request, 'readers/create_success.html', {'name': user.first_name, 'email': user.email})

    # if a GET (or any other method) we'll create a blank form
    else:
        formR = ReaderForm()
        formU = UserForm()

    # render form:
    return render(request, 'readers/create.html', {'formU': formU, 'formR': formR})

@login_required
def items_info(request):
    """
    Show reserved and booked books by Reader
    """
    user_now = request.user.username

    # Retrieve all user reservations and borrows

    if user_now != 'admin':
        reserves = Reserve.objects.filter(reader__user__username=request.user.username).order_by('date')
        borrows = Borrow.objects.filter(reader__user__username=request.user.username).order_by('date')
        nicks1 = [""]*Reserve.objects.count()
        nicks2 = [""]*Borrow.objects.count()
    else:
        all_reserves = Reserve.objects.order_by('date')
        reserves = Reserve.objects.order_by('date')
        borrows = Borrow.objects.order_by('date')
        nicks1 = [reserve.reader.user for reserve in all_reserves]
        nicks2 = [borrow.reader.user for borrow in borrows]
    # ha ha ha :P
    # trolololo pzdr od tomka
    sellings_info = SellingItem.objects.filter(user=request.user).order_by('date')
    queue_info = Queue.objects.filter(reader__user__username=request.user.username).order_by('date')


    reservation_dates = [reserve.date for reserve in reserves]
    # Extract consecutive dates of making a reservation and checkout
    borrow_dates = [borrow.date for borrow in borrows]


    # Fix some limit on checkout and return
    reservation_time_limit = 7
    borrow_time_limit = 30

    # For each book item calculate the dates, when it should be checkd out or returned
    reservation_expiry_dates = [reservation_date + datetime.timedelta(days=reservation_time_limit) for reservation_date in reservation_dates]
    borrow_expiry_dates = [borrow_date + datetime.timedelta(days=borrow_time_limit) for borrow_date in borrow_dates]

    # Zip the lists so they can be iterated over simultaneously in template
    reserve_info = zip(reserves, reservation_expiry_dates, nicks1)
    borrow_info = zip(borrows, borrow_expiry_dates, nicks2)

    print("borrow info: ", borrow_info)
    print("borrows: ", borrows)


    return render(request, 'readers/items_info.html',
        {
            'reserve_info': reserve_info,
            'borrow_info': borrow_info,
            'user_now': user_now,
            'nicks1': nicks1,
            'nicks2': nicks2,
            'sell_info': sellings_info,
            'queue_info': queue_info,
        }
    )

def users(request):
    readers = Reader.objects.order_by('id')
    user_dates = [reader for reader in readers]

    return render(request, 'readers/users.html', {'readers': user_dates})

@login_required
def sell_confirm_yes(request, id):
    item = SellingItem.objects.get(user=request.user, id=id)
    item.user_confirmed = True
    item.save()
    return redirect('readers.views.items_info')

@login_required
def sell_confirm_no(request, id):
    item = SellingItem.objects.get(user=request.user, id=id)
    item.user_confirmed = False
    item.save()
    return redirect('readers.views.items_info')