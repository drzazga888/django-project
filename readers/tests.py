from django.test import TestCase
from django.contrib.auth.models import User
from .forms import (ReaderForm, UserForm)
from books.models import *
from readers.models import *
import datetime

class ReadersViewsTestCase(TestCase):

    def setUp(self):
        self.user = User.objects.create_user('temporary', 'temporary@example.com','temporary')
        self.reader = Reader.objects.create(user=self.user, pesel="64011613697", address="ul. Krakowska 12/36", city="Kraków", postal_code="30-333")
        self.title = "Język C++. Szkoła programowania."
        self.authors = Author.objects.create(name="Stephen Prata")
        self.publisher = "Helion"
        self.year = 2012
        self.genre = Genre.objects.create(name="Programowanie")
        self.book = Book.objects.create(title=self.title, publisher=self.publisher, year=self.year)
        self.book.authors.add(self.authors)
        self.book.genre.add(self.genre)
        self.bookItem1 = BookItem.objects.create(book=self.book)
        self.date = datetime.date.today()

    def test_personal_info_logged_in(self):
        self.client.login(username="temporary", password="temporary")
        resp = self.client.get('/account/info/')
        self.assertEqual(resp.status_code, 200)
        self.assertTrue('reader' in resp.context)
        self.assertEqual(resp.context['reader'], self.reader)

    def test_personal_info_not_logged_in_redirects(self):
        resp = self.client.get('/account/info/')
        self.assertEqual(resp.status_code, 302)

    def test_create_blank_form(self):
        resp = self.client.get('/account/create/')
        self.assertEqual(resp.status_code, 200)
        formU = UserForm()
        formR = ReaderForm()
        self.assertFalse(formU.is_valid())
        self.assertFalse(formR.is_valid())
        self.assertTrue('formU' in resp.context)
        self.assertTrue('formR' in resp.context)

    def test_create_valid_form(self):
        dataU = {
            'first_name' : 'Jan',
            'last_name' : 'Kowalski',
            'username' : 'jkowalski',
            'email' : 'jan@kowalski.com',
            'password1' : 'pass',
            'password2' : 'pass',
        }
        dataR = {
            'pesel' : '62090305690',
            'city' : 'Krakow',
            'postal_code' : '30-333',
            'address' : 'ul. Krakowska 12/36'
        }
        formU = UserForm(data=dataU)
        formR = ReaderForm(data=dataR)
        self.assertTrue(formU.is_valid())
        self.assertTrue(formR.is_valid())

    def test_create_complete(self):
        data = {
            'first_name' : 'Jan',
            'last_name' : 'Kowalski',
            'username' : 'jkowalski',
            'email' : 'jan@kowalski.com',
            'password1' : 'pass',
            'password2' : 'pass',
            'pesel' : '62090305690',
            'city' : 'Krakow',
            'postal_code' : '30-333',
            'address' : 'ul. Krakowska 12/36'
        }
        resp = self.client.post('/account/create/', data)
        self.assertEqual(resp.status_code, 200)
        self.assertTrue('name' in resp.context)
        self.assertTrue('email' in resp.context)
        self.assertFalse('formU' in resp.context)
        self.assertFalse('formR' in resp.context)
        self.assertEqual(resp.context['name'], 'Jan')
        self.assertEqual(resp.context['email'], 'jan@kowalski.com')

    def test_create_blank_form_if_method_is_get(self):
        resp = self.client.get('/account/create/')
        self.assertTrue('formU' in resp.context)
        self.assertTrue('formR' in resp.context)

    def test_items_info_not_logged_in_redirects(self):
        resp = self.client.get('/account/items_info/')
        self.assertEqual(resp.status_code, 302)

    def test_items_info_reserved(self):
        self.client.login(username="temporary", password="temporary")
        reserved = Reserve(book_item=self.bookItem1, reader=self.reader, date=self.date).save()
        resp = self.client.get('/account/items_info/')
        self.assertEqual(resp.status_code, 200)
        for reserve, reservation_expiry_date in resp.context['reserve_info']:
            self.assertEqual(reserve, reserved)
            self.assertEqual(reservation_expiry_date, self.date + datetime.timedelta(days=7))

    def test_items_info_borrowed(self):
        self.client.login(username="temporary", password="temporary")
        borrowed = Borrow.objects.create(book_item=self.bookItem1, reader=self.reader, date=self.date).save()
        resp = self.client.get('/account/items_info/')
        self.assertEqual(resp.status_code, 200)
        for borrow, borrow_expiry_date in resp.context['borrow_info']:
            self.assertEqual(borrow, borrowed)
            self.assertEqual(borrow_expiry_date, self.date + datetime.timedelta(days=30))

    def test_items_info_no_items_borrowed_or_reserved(self):
        self.client.login(username="temporary", password="temporary")
        resp = self.client.get('/account/items_info/')
        self.assertEqual(resp.status_code, 200)
        self.assertTrue('reserve_info' in resp.context)
        self.assertTrue('borrow_info' in resp.context)
        self.assertEqual(len(list(resp.context['reserve_info'])), 0)
        self.assertEqual(len(list(resp.context['borrow_info'])), 0)