from django import forms
from django.forms import Textarea
from localflavor.pl.forms import PLPESELField
from localflavor.pl.forms import PLPostalCodeField
from django.contrib.auth.models import User
from .models import Reader, Suggestion, SellingItem
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Div
from crispy_forms.bootstrap import FormActions
from captcha.fields import CaptchaField


class ReaderForm(forms.ModelForm):
    # define Meta class to link this form with specific Model (Reader class)
    class Meta:
        model = Reader # specify Model
        fields = ['pesel', 'address', 'city', 'postal_code'] # which fields should be generated
        
    # All fields specified in 'fields' attribute of Meta class will be generated automatically.
    # However we want to substitute two of them by custom fields from localflavor package,
    # so we must add these two lines of code:
    pesel = PLPESELField()
    postal_code = PLPostalCodeField()
    captcha = CaptchaField(
        help_text="Code is no case-sensitive."
    )

    def __init__(self, *args, **kwargs):
        super(ReaderForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Div(
                Div(
                    'pesel',
                    'address',
                    css_class = 'col-md-6'
                ),

                Div(
                    'city',
                    'postal_code',
                    css_class = 'col-md-6'
                ),
                    css_class = 'row'
                ),
                Div(
                     Div(
                        'captcha',
                        css_class = 'col-md-6'
                     ),
                     Div(
                        css_class = 'col-md-6'
                     ),
                    Div(
                        FormActions(
                            Submit('submit', 'Submit'),
                            css_class = 'text-center',
                         ),
                        css_class='col-md-12'
                     ),
                     css_class='row'
                 )
        )


class UserForm(forms.ModelForm):
    # Implementation is based on source code of django.auth.forms.UserCreationForm class
    
    error_messages = {
        'duplicate_email': "A user with that email already exists.",
        'password_mismatch': "The two password fields didn't match.",
    }
    password1 = forms.CharField(
        label='Password',
        widget=forms.PasswordInput)
    password2 = forms.CharField(
        label='Password confirmation',
        widget=forms.PasswordInput,
        help_text="Enter the same password as above, for verification.")
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ('username',)

    def clean_password2(self):
        """
        Check if the second password is the same as the first one
        """
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2
    
    def clean_email(self):
        """
        Check if email dos not exist before
        """
        try:
            User.objects.get(email=self.cleaned_data['email']) # get user from user model
        except User.DoesNotExist :
            return self.cleaned_data['email']

        raise forms.ValidationError(
            self.error_messages['duplicate_email'],
            code='duplicate_email',
        )

    def save(self, commit=True):
        user = super(UserForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.email = self.cleaned_data['email']
        user.is_active = False
        if commit:
            user.save()
        return user

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Div(
                Div(
                'first_name',
                'last_name',
                'username',
                css_class = 'col-md-6'
                ),

                Div(
                'email',
                'password1',
                'password2',
                css_class = 'col-md-6'
                ),

            css_class = 'row'
            ),
        )
        self.fields['first_name'].widget.attrs['autofocus'] = 'autofocus'


class EmailForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(EmailForm, self).__init__(*args, **kwargs)

    email = forms.EmailField(required=True)
    error_messages = {
        'duplicate_email': "A user with that email already exists.",
    }

    class Meta:
        model = User
        fields = ('email',)

    def clean_email(self):
        try:
            User.objects.get(email=self.cleaned_data['email']) # get user from user model
        except User.DoesNotExist :
            return self.cleaned_data['email']

        raise forms.ValidationError(
                self.error_messages['duplicate_email'],
                code='duplicate_email',
            )

    def save(self, username):
        user = User.objects.get(username=username)
        user.email = self.cleaned_data.get('email')
        user.save()
        return user


class PasswordForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(PasswordForm, self).__init__(*args, **kwargs)

    password1 = forms.CharField(
        label='Password',
        widget=forms.PasswordInput)
    password2 = forms.CharField(
        label='Password confirmation',
        widget=forms.PasswordInput,
        help_text="Enter the same password as above, for verification.")

    error_messages = {
        'password_mismatch': "The two password fields didn't match.",
    }

    class Meta:
        model = User
        fields = ()

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def save(self, username):
        user = User.objects.get(username=username)
        user.set_password(self.cleaned_data.get('password1'))
        user.save()
        return user


class ChangeReaderForm(forms.ModelForm):
    # define Meta class to link this form with specific Model (Reader class)
    class Meta:
        model = Reader # specify Model
        fields = ['address', 'city', 'postal_code'] # which fields should be generated

    # All fields specified in 'fields' attribute of Meta class will be generated automatically.
    # However we want to substitute two of them by custom fields from localflavor package,
    # so we must add these two lines of code:
    postal_code = PLPostalCodeField()
    error_messages = {
        'password_mismatch': "The two password fields didn't match.",
    }

    def __init__(self, *args, **kwargs):
        super(ChangeReaderForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Div(
                Div(
                'address',
                css_class = 'col-md-6'
                ),

                Div(
                'city',
                'postal_code',
                css_class = 'col-md-6'
                ),
            css_class = 'row'
            ),
            FormActions(
            Submit('submit', 'Change'),
            css_class = 'text-center',
            ),
        )

    def save(self, user):
        reader = Reader.objects.get(user_id=user)
        reader.address = self.cleaned_data['address']
        reader.postal_code = self.cleaned_data['postal_code']
        reader.city = self.cleaned_data['city']
        reader.save()
        return reader


class ChangeUserForm(forms.ModelForm):

    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)

    class Meta:
        model = User
        fields = ('first_name','last_name')

    def save(self, user):
        user = User.objects.get(username=user)
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.save()
        return user


    def __init__(self, *args, **kwargs):
        super(ChangeUserForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_tag = False
        self.helper.layout = Layout(

            Div(
                Div(
                'first_name',
                css_class = 'col-md-6'
                ),

                Div(
                'last_name',
                css_class = 'col-md-6'
                ),
            css_class = 'row'
            ),
        )

        self.fields['first_name'].widget.attrs['autofocus'] = 'autofocus'


class SellingItemForm(forms.ModelForm):

    class Meta:
        model = SellingItem
        exclude = ('user', 'admin_confirmed', 'user_confirmed', 'proposition_date')

    def __init__(self, *args, **kwargs):
        super(SellingItemForm, self).__init__(*args, **kwargs)
        for key in self.fields:
            self.fields[key].widget.attrs['required'] = "required"
            self.fields[key].widget.attrs['class'] = 'form-control'

    def save(self, id, commit=True):
        instance = super(SellingItemForm, self).save(commit=False)
        instance.user = User.objects.get(id=id)
        instance.admin_confirmed = False
        instance.user_confirmed = False
        if commit:
            instance.save()
        return instance


class SuggestionForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(SuggestionForm, self).__init__(*args, **kwargs)
        for key in self.fields:
            self.fields[key].widget.attrs['required'] = "required"
            self.fields[key].widget.attrs['class'] = 'form-control'

    class Meta:
        model = Suggestion
        fields = ['book_title', 'book_url', 'suggest_reason']
        widgets = {
            'suggest_reason': Textarea(attrs={'rows': 4}),
        }
        labels = {
            'book_title': 'Book title',
            'book_url': 'Reference link to book',
            'suggest_reason': 'Why are you suggesting the book?'
        }

    def save(self, id, commit=True):
        instance = super(SuggestionForm, self).save(commit=False)
        instance.user = User.objects.get(id=id)
        if commit:
            instance.save()
        return instance