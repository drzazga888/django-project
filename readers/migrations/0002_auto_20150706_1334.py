# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('readers', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='SellingItem',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('book_title', models.CharField(max_length=512)),
                ('book_authors', models.CharField(max_length=512)),
                ('book_publisher', models.CharField(max_length=128)),
                ('book_url', models.URLField(max_length=512)),
                ('book_status', models.IntegerField(choices=[(5, 'Excellent'), (4, 'Good'), (3, 'Normal'), (2, 'Bad'), (1, 'Ugly')])),
                ('book_price', models.DecimalField(max_digits=6, decimal_places=2)),
                ('date', models.DateField(auto_now_add=True)),
                ('admin_confirmed', models.BooleanField(default=False)),
                ('user_confirmed', models.BooleanField(default=False)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Suggestion',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('book_title', models.CharField(max_length=512)),
                ('book_url', models.CharField(max_length=512)),
                ('suggest_reason', models.CharField(max_length=512)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AlterField(
            model_name='reader',
            name='pesel',
            field=models.CharField(max_length=11),
        ),
    ]
